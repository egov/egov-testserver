<?php
/**
 * Created by PhpStorm.
 * User: curtisk
 * Date: 10/22/18
 * Time: 8:32 AM
 */

echo '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>eGov Testing</title></head>';
echo '<body><div><center><img src="images/splash.jpg"></center></div>';
echo '<span><center><br><fieldset style="width: 90%;border: none;">';
echo '<table cellpadding="10" cellspacing="0" border="0" align="center">';
echo  "<tr><th bgcolor='#CCCCCC'>Instance</th><th  bgcolor='#CCCCCC'>Current Tag</th><th  bgcolor='#CCCCCC'>Jira Ticket</th></tr>";

$directory = scandir('clients/');
$count =0;

foreach($directory as $file) {
    if (!is_dir($file)) {
        echo '<tr>';
        if(file_exists('images/'.$file .'.png')) {
            echo "<td  style='border-bottom: 1px solid black' align='left' width='33%'><img width='437' height='131' src='images/$file.png' alt='$file.png'></td>";
        }else{
            echo "<td style='border-bottom: 1px solid black' align='left' width='33%'><img width='437' height='131' src='images/$file.jpg' alt='$file.jpg'></td>";
        }

        $str = "git --git-dir clients/$file/.git  symbolic-ref -q --short HEAD || git --git-dir clients/$file/.git describe --tags --exact-match";
        $version = exec($str);
        $client = ucwords($file);
        if($version != ""){
            echo "<td style='border-bottom: 1px solid black' align='left' width='33%' valign='top'><strong>$client</strong> <br>VERSION:&nbsp;<a href='clients/$file'>$version</a></td>";
        }else{
            echo "<td style='border-bottom: 1px solid black' align='left' width='33%' valign='top'><strong>$client</strong> <br>NO VERSION PRESENT</td>";

        }

        echo "<td  style='border-bottom: 1px solid black' width='33%'></td>";
        echo '</tr>';
    }
}

echo '</table></fieldset></center></span></body></html>';
